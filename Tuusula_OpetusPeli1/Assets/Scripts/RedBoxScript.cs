﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBoxScript : MonoBehaviour
{
    private Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        if(rb.velocity == Vector3.zero)
        {
            rb.isKinematic = true;
            Destroy(this);
        }
    }
}
