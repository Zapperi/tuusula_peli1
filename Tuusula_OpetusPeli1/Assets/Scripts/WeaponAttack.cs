﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAttack : MonoBehaviour
{
    public bool isMeleeWeapon;
    public bool isRangedWeapon;
    public void DoAttack()
    {
        if (isMeleeWeapon){
            GetComponent<MeleeWeapon>().DoMeleeAttack();
        }
        else if (isRangedWeapon)
        {
            GetComponent<RangedWeapon>().FireBullet(); 
        }
    }
}
