﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody rb;
    private ParticleSystem shieldCollisionParticle;
    public AudioClip shieldtHitSound;
    public AudioClip shellCasingSound;
    public AudioSource audioSource;
    private bool bulletHit;
    private bool despawning;

    public void FireBullet(float force, float spread)
    {
        transform.Rotate(Random.Range(-spread, spread), Random.Range(-spread, spread), 0.0f);
        rb.AddForce(transform.forward * force, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!despawning)
        {
            Destroy(gameObject, 3);
            despawning = true;
        }

        if(collision.collider.tag == "Shield")
        {
            shieldCollisionParticle = Instantiate(Resources.Load<ParticleSystem>("BulletShieldHit_Particle"), transform.position, transform.rotation, null);
            shieldCollisionParticle.Play();
            Destroy(shieldCollisionParticle.gameObject, shieldCollisionParticle.main.duration);
            audioSource.clip = shieldtHitSound;
            audioSource.pitch = Random.Range(audioSource.pitch - 0.1f, audioSource.pitch + 0.3f);
            audioSource.Play();
        }
        else if(collision.collider.tag == "Player" && !bulletHit)
        {
            //DoDamage();
        }
        else
        {
            bulletHit = true;
            AudioSource shellBounce = gameObject.AddComponent<AudioSource>();
            shellBounce.clip = shellCasingSound;
            shellBounce.volume = 0.1f;
            shellBounce.pitch = Random.Range(shellBounce.pitch - 0.1f, shellBounce.pitch + 0.1f);
            shellBounce.Play();
        }        
    }
}
