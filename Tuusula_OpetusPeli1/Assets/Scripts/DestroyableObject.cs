﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObject : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem destroyedParticles;
    [SerializeField]
    private AudioClip destroyedSound;
    [SerializeField]
    private AudioSource audioSource;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Box hit by: " + other.gameObject.tag);
        if((other.gameObject.tag == "MeleeWeapon" && PlayerController.Instance.playerAttacking) || other.tag == "Bullet")
        {
            destroyedParticles.Play();
            audioSource.clip = destroyedSound;
            audioSource.pitch = (Random.Range(audioSource.pitch - 0.2f, audioSource.pitch + 0.2f));
            audioSource.Play();
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            Destroy(this, destroyedParticles.main.duration);
        }
    }
}
