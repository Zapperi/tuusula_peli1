﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RangedWeapon : MonoBehaviour
{
    public Transform bulletSpawnPoint;
    public Transform grabPoint;
    public AudioClip gunFireSound;
    public List<AudioSource> audioSourceList = new List<AudioSource>();
    public float fireForce;
    public float spreadValue;
    private GameObject bulletPrefab;

    private float gunFireOriginalPitch;

    private void Awake()
    {
        gunFireOriginalPitch = audioSourceList[0].pitch;
    }
    public void FireBullet()
    {
        for(int i = 0; i < audioSourceList.Count; ++i)
        {
            if (!audioSourceList[i].isPlaying)
            {
                AudioSource temp = audioSourceList[i];
                temp.pitch = Random.Range(gunFireOriginalPitch - 0.1f, gunFireOriginalPitch + 0.1f);
                temp.clip = gunFireSound;
                temp.Play();
                break;
            }
            else if (i == audioSourceList.Count -1)
            {
                audioSourceList.Add(gameObject.AddComponent<AudioSource>());
                AudioSource temp = audioSourceList.Last();
                temp.pitch = Random.Range(gunFireOriginalPitch - 0.1f, gunFireOriginalPitch + 0.1f);
                temp.clip = gunFireSound;
                temp.Play();
                break;
            }
        }        
        GameObject bullet = Instantiate(Resources.Load("Bullet") as GameObject, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        ParticleSystem particles = Instantiate(Resources.Load<ParticleSystem>("GunShot_Particle"), bulletSpawnPoint.position, bulletSpawnPoint.rotation, bulletSpawnPoint);
        particles.Play();
        if (!PlayerController.Instance.playerAttacking)
        {
            //Debug.Log("Player melee attack started!");
            PlayerController.Instance.playerAttacking = true;
            PlayerController.Instance.playerAnimator.Play("PlayerShoot");
        }
        else if (PlayerController.Instance.playerAttacking)
        {
            //Debug.Log("Player already melee attacking!");
        }
        bullet.GetComponent<Bullet>().FireBullet(fireForce, spreadValue);
    }
}
