﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6.0f;
    public float jumpStrengh = 8.0f;
    public float turnSpeed = 5.0f;
    public float gravity = -9.81f;

    private string moveInputAxis = "Vertical";
    private string turnInputAxis = "Horizontal";

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 turnDirection = Vector3.zero;

    void Update()
    {
        if (PlayerController.Instance.isGrounded)
        {           
            moveDirection = new Vector3(0f, 0f, Input.GetAxis(moveInputAxis));
            moveDirection *= speed;

            if (Input.GetButtonDown("Jump"))
            {
                PlayerController.Instance.PlayerJump(jumpStrengh);
            }            
        }

        if (Input.GetButton("Fire2"))
        {
            PlayerController.Instance.PlayerDefend();
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            PlayerController.Instance.PlayerFinishDefend();
        }
        else if (Input.GetButtonDown("Fire1"))
        {
            PlayerController.Instance.PlayerAttack();
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            PlayerController.Instance.PickUpWeapon();
        }

        turnDirection = new Vector3(0f, Input.GetAxis(turnInputAxis), 0f);
        turnDirection *= turnSpeed;
        
        // Move the player
        PlayerController.Instance.MovePlayer(moveDirection * Time.deltaTime);
        PlayerController.Instance.TurnPlayer(turnDirection * Time.deltaTime);
    }
}
