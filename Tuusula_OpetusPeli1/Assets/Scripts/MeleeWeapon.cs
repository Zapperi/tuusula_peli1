﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : MonoBehaviour
{
    public Transform grabPoint;
    [Range(1,10)]
    public float attackSpeed;
    public AnimationClip attackAnimation;
    public void DoMeleeAttack()
    {
        if (!PlayerController.Instance.playerAttacking)
        {
            //Debug.Log("Player melee attack started!");
            PlayerController.Instance.playerAttacking = true;
            PlayerController.Instance.playerAnimator.SetFloat("MeleeSpeed", attackSpeed);
            PlayerController.Instance.playerAnimator.Play(attackAnimation.name);
        }
        else if (PlayerController.Instance.playerAttacking)
        {
            //Debug.Log("Player already melee attacking!");
        }
    }
}
