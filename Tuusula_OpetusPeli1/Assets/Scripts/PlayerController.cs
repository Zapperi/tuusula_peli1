﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;
    public GameObject playerWeapon;
    private Rigidbody m_player_rb;
    private Transform m_player_transform;
    public Animator playerAnimator;
    public GameObject playerRightHand;

    public bool isGrounded = true;
    public bool playerAttacking = false;
    public GameObject objectInTriggerZone;

    // Start is called before the first frame update
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        m_player_rb = GetComponent<Rigidbody>();
        m_player_transform = GetComponent<Transform>();        
    }

    public void MovePlayer(Vector3 moveDirection)
    {
        m_player_transform.Translate(moveDirection);
    }

    public void TurnPlayer(Vector3 turnDirection)
    {
        m_player_transform.Rotate(turnDirection);
    }

    public void PlayerJump(float jumpStrenght)
    {
        m_player_rb.AddForce(m_player_transform.up * jumpStrenght * m_player_rb.mass, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "floor")
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.gameObject.tag == "floor")
        {
            isGrounded = false;
        }
    }

    public void PlayerAttack()
    {
        if (!playerWeapon)
        {
            return;
        }
        playerWeapon.GetComponent<WeaponAttack>().DoAttack();
    }

    public void PlayerDefend()
    {
        playerAnimator.Play("PlayerDefend");
    }
    public void PlayerFinishDefend()
    {
        playerAnimator.Play("Idle");
    }

    public void FinishPlayerAttack()
    {
        playerAttacking = false;
    }

    public void PickUpWeapon()
    {
        if (!objectInTriggerZone)
        {
            return;
        }

        GameObject temp = objectInTriggerZone.transform.parent.gameObject;
        if (playerWeapon)
        {
            playerWeapon.transform.parent = null;
            playerWeapon.GetComponent<Rigidbody>().isKinematic = false;
            playerWeapon.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-3.0f, 3.0f), 5f, Random.Range(-3.0f, 3.0f)), ForceMode.Impulse);
        }
        playerWeapon = temp;
        playerWeapon.GetComponent<Rigidbody>().isKinematic = true;
        playerWeapon.transform.parent = playerRightHand.transform;
        playerWeapon.transform.localPosition = Vector3.zero;
        playerWeapon.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    public void UpdatePlayerWeapon(GameObject newWeapon)
    {
        playerWeapon.transform.parent = null;
        playerWeapon = newWeapon;
        playerWeapon.transform.parent = playerRightHand.transform;
        playerWeapon.transform.localPosition = Vector3.zero;
        playerWeapon.transform.localEulerAngles = Vector3.zero;
    }

    private void OnTriggerEnter(Collider other)
    {        
        if(other.tag == "MeleeWeapon" || other.tag == "RangedWeapon")
        {
            Debug.Log("Weapon entered zone!");
            objectInTriggerZone = other.transform.parent.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "MeleeWeapon" || other.gameObject.tag == "RangedWeapon")
        {
            objectInTriggerZone = null;
        }
    }
}
