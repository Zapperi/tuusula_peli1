﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [Tooltip("First object to spawn.")]
    public GameObject firstObject;
    [Tooltip("Second object to spawn.")]
    public GameObject secondObject;
    [Tooltip("First object ammount.")]
    public int firstObjectAmmount;
    [Tooltip("Second object ammount.")]
    public int secondObjectAmmount;
    [Tooltip("Buffer distance from the floor bounds.")]
    [Range(0f, 30f)]
    public float bufferSize = 5f;

    private Vector3 spawnPosition;
    private float floorMinWidth;
    private float floorMaxWidth;
    private float floorMinLenght;
    private float floorMaxLenght;
    // Start is called before the first frame update
    void Start()
    {
        InitSpawnBoundaries();

        for (int i = 0; firstObjectAmmount >= i; ++i)
        {
            spawnPosition = new Vector3 (Random.Range(floorMinWidth, floorMaxWidth), 30f, Random.Range(floorMinLenght, floorMaxLenght));
            Instantiate(firstObject, spawnPosition, Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360))), this.transform);
        }
        for (int i = 0; secondObjectAmmount >= i; ++i)
        {
            spawnPosition = new Vector3(Random.Range(floorMinWidth, floorMaxWidth), 30f, Random.Range(floorMinLenght, floorMaxLenght));
            Instantiate(secondObject, spawnPosition, Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360))), this.transform);
        }
    }

    /// <summary>
    /// Calculate object spawn area according to floor's bounds.
    /// </summary>
    private void InitSpawnBoundaries()
    {
        floorMinLenght = GameController.Instance.floor.GetComponent<MeshRenderer>().bounds.min.x + bufferSize;
        floorMaxLenght = GameController.Instance.floor.GetComponent<MeshRenderer>().bounds.max.x - bufferSize;
        floorMinWidth = GameController.Instance.floor.GetComponent<MeshRenderer>().bounds.min.z + bufferSize;
        floorMaxWidth = GameController.Instance.floor.GetComponent<MeshRenderer>().bounds.max.z - bufferSize;
    }
}
